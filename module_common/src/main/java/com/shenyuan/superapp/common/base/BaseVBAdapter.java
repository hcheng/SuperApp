package com.shenyuan.superapp.common.base;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.shenyuan.superapp.common.R;

import org.jetbrains.annotations.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author ch
 * @date 2021/2/20 11:54
 * desc
 */
public class BaseVBAdapter<T, VB extends ViewDataBinding> extends BaseQuickAdapter<T, BaseDataBindingHolder<VB>> {
    /**
     * 是否需要空view
     */
    protected boolean needEmptyView = true;

    public void setNeedEmptyView(boolean needEmptyView) {
        this.needEmptyView = needEmptyView;
    }

    public BaseVBAdapter(int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }

    public BaseVBAdapter(int layoutResId, @Nullable List<T> data, boolean needEmptyView) {
        super(layoutResId, data);
        this.needEmptyView = needEmptyView;
    }

    public BaseVBAdapter(int layoutResId, boolean needEmptyView) {
        super(layoutResId);
        this.needEmptyView = needEmptyView;
    }

    public BaseVBAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(@NonNull BaseDataBindingHolder<VB> holder, T item, @NonNull List<?> payloads) {
        super.convert(holder, item, payloads);
    }

    @Override
    public void setNewInstance(@Nullable List<T> list) {
        super.setNewInstance(list);
        setCustomEmptyView(list, needEmptyView);
    }

    /**
     * 设置数据
     *
     * @param list          数据
     * @param needEmptyView 是否需要
     */
    public void setNewInstance(@Nullable List<T> list, boolean needEmptyView) {
        super.setNewInstance(list);
        this.needEmptyView = needEmptyView;
        setCustomEmptyView(list, needEmptyView);
    }

    /**
     * 是否需要设置 空布局
     *
     * @param list          数据
     * @param needEmptyView 是否需要
     */
    private void setCustomEmptyView(List<T> list, boolean needEmptyView) {
        if (list == null || list.size() == 0) {
            if (needEmptyView) {
                setEmptyView(getViewByRes(R.layout.empty_no_data));
            }
        }
    }

    /**
     * 设置点击事件
     *
     * @param listener listener
     */
    public void setClickListener(ItemClickListener<T> listener) {
        if (listener == null) {
            return;
        }
        setOnItemClickListener((adapter, view, position) -> listener.onItemClick(getItem(position), view, position));
    }

    /**
     * @param color color
     * @return int
     */
    public int getValuesColor(int color) {
        return getContext().getResources().getColor(color);
    }

    /**
     * 通过资源res获得view
     *
     * @param res res
     * @return View
     */
    public View getViewByRes(@LayoutRes int res) {
        return LayoutInflater.from(getContext()).inflate(res, null);
    }


    /**
     * @param s s
     */
    public void showToast(@NonNull String s) {
        Toast toast = Toast.makeText(getContext(), s, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    protected void convert(@NonNull BaseDataBindingHolder<VB> holder, T t) {

    }


    public interface ItemClickListener<T> {
        /**
         * 点击事件
         *
         * @param bean     bean
         * @param view     view
         * @param position position
         */
        void onItemClick(@NonNull T bean, @NonNull View view, int position);
    }

    /**
     * 反射获取 ViewDataBinding
     *
     * @return ViewDataBinding
     */
    private VB getBinding() {
        Class classz = getBingingClass();
        if (classz == null) {
            return null;
        }
        try {
            return (VB) classz.getMethod("inflate", LayoutInflater.class).invoke(null, LayoutInflater.from(getContext()));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取Binging的泛型
     *
     * @return Class
     */
    private Class getBingingClass() {
        if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
            Type[] types = parameterizedType.getActualTypeArguments();
            if (types.length > 0) {
                return (Class) types[1];
            }
        }
        return null;
    }
}

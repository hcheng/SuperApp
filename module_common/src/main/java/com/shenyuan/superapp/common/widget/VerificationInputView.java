package com.shenyuan.superapp.common.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.shenyuan.superapp.base.utils.DensityUtils;
import com.shenyuan.superapp.common.R;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by _hxb on 2018/8/6 0006.
 */
public class VerificationInputView extends LinearLayout {

    private int mChildLeftMargin;
    private int mChildRightMargin;
    private int mChildWidth;
    private int mChildHeight;
    private Drawable mChildBgFocus;
    private Drawable mChildBgNormal;
    private int mChildCount;
    private Listener listener;
    private IndexListener mIndexListener;
    private List<LastInputEditText> mEditTextList = new ArrayList<>();
    private boolean mChildCanClickable = true;

    public interface Listener {
        public void onChange(String[] strings, List<LastInputEditText> editTextList);

        public void onComplete(String string);
    }

    public interface IndexListener {
        public void getIndex(int index);
    }

    public VerificationInputView(Context context) {
        this(context, null);

    }

    public VerificationInputView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public VerificationInputView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(HORIZONTAL);
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.VerificationInputView);
        a.getInt(R.styleable.VerificationInputView_child_count, 4);

        mChildLeftMargin = (int) a.getDimension(R.styleable.VerificationInputView_child_left_margin, 0);
        mChildRightMargin = (int) a.getDimension(R.styleable.VerificationInputView_child_left_margin, 0);
        mChildWidth = (int) a.getDimension(R.styleable.VerificationInputView_child_width, 0);
        mChildHeight = (int) a.getDimension(R.styleable.VerificationInputView_child_height, 0);
        mChildBgFocus = a.getDrawable(R.styleable.VerificationInputView_child_bg_focus);
        mChildBgNormal = a.getDrawable(R.styleable.VerificationInputView_child_bg_normal);
        mChildCount = a.getInt(R.styleable.VerificationInputView_child_count, 6);
        a.recycle();
        addView();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setIndexListener(IndexListener listener) {
        this.mIndexListener = listener;
    }

    public List<LastInputEditText> getEditTextList() {
        return mEditTextList;
    }

    public void setChildCanClickable(boolean childCanClickable) {
        mChildCanClickable = childCanClickable;
        if (!childCanClickable) {
            setOnClickListener(v -> {
                for (int i = 0; i < mEditTextList.size(); i++) {
                    EditText editText = mEditTextList.get(i);
                    String string = editText.getText().toString();
                    if (string.length() == 0 || i == mEditTextList.size() - 1) {
                        editText.requestFocus();
                        break;
                    }
                }
            });
        } else {
            setOnClickListener(null);
        }

    }

    public void setCursorVisible(boolean visible) {
        for (int i = 0; i < mEditTextList.size(); i++) {
            EditText editText = mEditTextList.get(i);
            editText.setCursorVisible(visible);
        }
    }

    public void clear() {
        for (int i = mEditTextList.size() - 1; i >= 0; i--) {
            LastInputEditText editText = mEditTextList.get(i);
            editText.setText("");

            if (i == 0) {
                editText.requestFocus();
                setBg(editText, true);
            } else {
                setBg(editText, false);
            }
        }
    }

    public void setChildInputType(int inputType) {
        for (EditText editText : mEditTextList) {
            editText.setInputType(inputType);
        }
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mChildCanClickable) {
            return super.onInterceptTouchEvent(ev);
        } else {
            return true;
        }
    }


    private void addView() {
        for (int i = 0; i < mChildCount; i++) {
            final LastInputEditText editText = new LastInputEditText(getContext());
            LayoutParams layoutParams = new LayoutParams(mChildWidth, mChildHeight);
            layoutParams.weight = 1;
            if (i == 0) {
                layoutParams.leftMargin = 0;
            } else {
                layoutParams.leftMargin = mChildLeftMargin;
            }
            if (i == mChildCount - 1) {
                layoutParams.rightMargin = 0;
            } else {
                layoutParams.rightMargin = mChildRightMargin;
            }
            editText.setLayoutParams(layoutParams);
            if (i == 0) {
                setBg(editText, true);
            } else {
                setBg(editText, false);
            }
            setCursorDrawableColor(editText, getResources().getColor(R.color.color_EDEDED));
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            editText.setGravity(Gravity.CENTER);
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
            editText.setTextColor(Color.BLACK);
            editText.setTag(i);
            editText.setTextSize(18);
            editText.addTextChangedListener(new MyTextWatcher(editText));
            editText.setOnKeyListener(new MyKeyListener(editText));
            editText.setBackgroundResource(R.drawable.et_underline_selector);
            editText.setOnFocusChangeListener((v, hasFocus) -> {
                setBg(editText, hasFocus);
                editText.setSelection(editText.getText().toString().length());
            });

            editText.setOnFocusChangeListener((v, hasFocus) -> {
                if (mIndexListener != null) {
                    if (hasFocus) {
                        mIndexListener.getIndex((Integer) editText.getTag());
                    }
                }
            });

            addView(editText, i);
            mEditTextList.add(editText);
        }
    }

    @SuppressWarnings("deprecation")
    private void setBg(LastInputEditText editText, boolean focus) {
        if (mChildBgNormal == null || mChildBgFocus == null) {
            return;
        }

        if (!focus) {
            if (editText.getText().toString().length() == 1) {
                editText.setBackgroundDrawable(mChildBgFocus);
            } else {
                editText.setBackgroundDrawable(mChildBgNormal);
            }
        } else {
            editText.setBackgroundDrawable(mChildBgFocus);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private EditText mEditText;


        public MyTextWatcher(LastInputEditText editText) {
            mEditText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() == 1) {
                int index = mEditTextList.indexOf(mEditText);
                if (index != mEditTextList.size() - 1) {
                    LastInputEditText editText = mEditTextList.get(index + 1);
                    editText.requestFocus();
                    setBg(editText, true);
                }
            }
            notifyDataChanged();
        }

    }

    private class MyKeyListener implements OnKeyListener {

        private EditText mEditText;


        public MyKeyListener(EditText editText) {
            mEditText = editText;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                int length = mEditText.getText().toString().length();
                if (length == 1) {
                    mEditText.setText("");
                } else {
                    int index = mEditTextList.indexOf(mEditText);
                    if (index != 0) {
                        EditText editText = mEditTextList.get(index - 1);
                        editText.requestFocus();
                        editText.setText("");
                    }
                }
                notifyDataChanged();
                return true;
            }
            return false;
        }
    }

    private void notifyDataChanged() {

        if (listener == null) {
            return;
        }
        String[] strings = new String[mChildCount];
        boolean complete = true;
        for (int i = 0; i < mEditTextList.size(); i++) {
            EditText editText = mEditTextList.get(i);
            String content = editText.getText().toString();
            strings[i] = content;
            if (content.length() == 0) {
                complete = false;
            }
        }
        listener.onChange(strings, mEditTextList);
        if (complete) {
            StringBuilder sb = new StringBuilder();
            for (String string : strings) {
                sb.append(string);
            }
            listener.onComplete(sb.toString());
        }
    }

    public void setCursorDrawableColor(EditText editText, int color) {
        try {
            Field fCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");//获取这个字段
            fCursorDrawableRes.setAccessible(true);//代表这个字段、方法等等可以被访问
            int mCursorDrawableRes = fCursorDrawableRes.getInt(editText);

            Field fEditor = TextView.class.getDeclaredField("mEditor");
            fEditor.setAccessible(true);
            Object editor = fEditor.get(editText);

            Class<?> clazz = editor.getClass();
            Field fCursorDrawable = clazz.getDeclaredField("mCursorDrawable");
            fCursorDrawable.setAccessible(true);

            Drawable[] drawables = new Drawable[2];
            drawables[0] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[1] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[0].setColorFilter(color, PorterDuff.Mode.SRC_IN);//SRC_IN 上下层都显示。下层居上显示。
            drawables[1].setColorFilter(color, PorterDuff.Mode.SRC_IN);
            fCursorDrawable.set(editor, drawables);
        } catch (Throwable ignored) {
        }
    }

}

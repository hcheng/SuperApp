package com.shenyuan.module_basekt.api.fastjson

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * @author ch
 * @date 2022/7/15-14:21
 * desc
 */
class FastJsonConverterFactory : Converter.Factory() {
    override fun requestBodyConverter(type: Type, parameterAnnotations: Array<out Annotation>, methodAnnotations: Array<out Annotation>, retrofit: Retrofit): Converter<*, RequestBody>? {
        return FastJsonResponseBodyConverter(type)
    }

    override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit): Converter<ResponseBody, *>? {
        return FastJsonRequestBodyConverter()
    }
}
package com.shenyuan.module_basekt.api

import retrofit2.Call
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * @author ch
 * @date 2022/7/13-18:23
 * desc
 */
interface ApiServer {
    @FormUrlEncoded
    @POST("auth/oauth/token")
    fun refreshToken(@FieldMap map: HashMap<String, String>): Call<HashMap<String, String>>
}
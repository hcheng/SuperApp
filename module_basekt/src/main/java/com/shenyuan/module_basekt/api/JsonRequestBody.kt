package com.shenyuan.module_basekt.api

import okhttp3.MediaType
import okhttp3.RequestBody
import okio.BufferedSink
import java.nio.charset.StandardCharsets

/**
 * @author ch
 * @date 2022/8/12-15:04
 * desc
 */
class JsonRequestBody : RequestBody() {

    val content: String = ""




    override fun contentType(): MediaType? {
        return MediaType.parse("application/json; charset=utf-8")
    }

    override fun writeTo(sink: BufferedSink) {
        val charset = StandardCharsets.UTF_8
        val bytes = content.toByteArray(charset)
        sink.write(bytes, 0, bytes.size)
    }
}
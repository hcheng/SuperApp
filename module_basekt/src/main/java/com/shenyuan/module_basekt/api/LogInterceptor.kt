package com.shenyuan.module_basekt.api

import com.shenyuan.module_basekt.utils.AppUtils
import com.shenyuan.module_basekt.utils.LogUtils
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody

/**
 * @author ch
 * @date 2022/7/13-17:10
 * desc
 */
class LogInterceptor : Interceptor {
    private val TAG = "ApiRetrofit"
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val startTime = System.currentTimeMillis()
        val response = chain.proceed(chain.request())
        val endTime = System.currentTimeMillis()
        val duration = endTime - startTime;
        val mediaType = response.body()?.contentType()
        val content = response.body()?.string()


        LogUtils.e(TAG, "----------Request Start----------------")
        LogUtils.e(TAG, "| " + request.toString() + request.headers().toString())
        LogUtils.e(TAG, "| Response:" + AppUtils.unicodeToutf8(content))
        LogUtils.e(TAG, "----------Request End:" + duration + "毫秒----------")
        return response.newBuilder().body(ResponseBody.create(mediaType, content)).build()
    }
}
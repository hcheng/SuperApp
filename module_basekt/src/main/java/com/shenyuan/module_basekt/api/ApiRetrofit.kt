package com.shenyuan.module_basekt.api

import com.shenyuan.module_basekt.api.fastjson.FastJsonConverterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.net.Proxy
import java.util.concurrent.TimeUnit

/**
 * @author ch
 * @date 2022/7/13-18:03
 * desc
 */
class ApiRetrofit {
    val retrofit by lazy {
        val client = OkHttpClient.Builder()
                .addInterceptor(LogInterceptor())
                .proxy(Proxy.NO_PROXY)
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build()

        Retrofit.Builder()
                .baseUrl("")
                .client(client)
                .addConverterFactory(FastJsonConverterFactory())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    init {

    }

    object ApiRetrofit {

    }

    fun <T> getService(serviceCls: Class<T>): T {
        return retrofit.create(serviceCls)
    }
}
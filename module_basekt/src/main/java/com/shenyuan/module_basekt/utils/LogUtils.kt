package com.shenyuan.module_basekt.utils

import android.util.Log

/**
 * @author ch
 * @date 2022/7/13-17:37
 * desc log
 */
class LogUtils {
    companion object {
        private val ALL = true
        private val I = true
        private val D = true
        private val E = true
        private val V = true
        private val W = true
        private val DEFAULT_TAG = "cheng"

        /**
         * i
         */
        fun i(tag: String, msg: String) {
            if (ALL && I) {
                Log.i(tag, msg)
            }
        }

        /**
         * i
         *
         * @param msg msg
         */
        fun i(msg: String) {
            i(DEFAULT_TAG, msg)
        }

        /**
         * d
         */
        fun d(tag: String, msg: String) {
            if (ALL && D) {
                Log.d(tag, msg)
            }
        }

        /**
         * d
         */
        fun d(msg: String) {
            d(DEFAULT_TAG, msg)
        }

        /**
         * e
         */

        fun e(tag: String, msg: String) {
            if (ALL && E) {
                Log.e(tag, msg)
            }
        }

        /**
         * e
         */
        fun e(msg: String) {
            e(DEFAULT_TAG, msg)
        }

        /**
         * v
         */
        fun v(tag: String, msg: String) {
            if (ALL && V) {
                Log.v(tag, msg)
            }
        }

        /**
         * v
         */
        fun v(msg: String) {
            v(DEFAULT_TAG, msg)
        }

        /**
         * w
         */
        fun w(tag: String, msg: String) {
            if (ALL && W) {
                Log.w(tag, msg)
            }
        }

        /**
         * v
         */
        fun w(msg: String) {
            w(DEFAULT_TAG, msg)
        }
    }
}
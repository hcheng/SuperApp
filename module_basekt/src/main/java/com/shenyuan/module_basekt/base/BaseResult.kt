package com.shenyuan.module_basekt.base

/**
 * @author ch
 * @date 2022/7/19-10:42
 * desc
 */
data class BaseResult<T>(val code: String, val msg: String?, val data: T?)

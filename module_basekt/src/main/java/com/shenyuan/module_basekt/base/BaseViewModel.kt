package com.shenyuan.module_basekt.base

import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author ch
 * @date 2022/7/18-14:30
 * desc
 */
class BaseViewModel : ViewModel() {


    fun <T> addDisposable(flowable: Flowable<T>, subscriber: BaseVMSubscriber<T>): BaseLiveData<BaseResult<T>> {
        flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber)
        return subscriber.liveData
    }
}
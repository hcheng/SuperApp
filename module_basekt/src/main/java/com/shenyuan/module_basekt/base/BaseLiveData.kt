package com.shenyuan.module_basekt.base

import androidx.lifecycle.MutableLiveData

/**
 * @author ch
 * @date 2022/7/18-14:47
 * desc
 */
class BaseLiveData<T> : MutableLiveData<T>() {
}
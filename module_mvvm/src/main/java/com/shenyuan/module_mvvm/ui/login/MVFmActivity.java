package com.shenyuan.module_mvvm.ui.login;

import androidx.lifecycle.Lifecycle;

import com.shenyuan.module_mvvm.R;
import com.shenyuan.module_mvvm.databinding.AcVmFmBinding;
import com.shenyuan.superapp.base.base.mvvm.BaseVMActivity;
import com.shenyuan.superapp.base.base.mvvm.BaseViewModel;

/**
 * @author ch
 * @date 2022/6/13-15:18
 * desc
 */
public class MVFmActivity extends BaseVMActivity<AcVmFmBinding, BaseViewModel> {
    private MVFragment fm1;
    private MVFragment fm2;
    private MVFragment fm3;
    private MVFragment fm4;
    private MVFragment fm5;

    @Override
    protected BaseViewModel createViewModel() {
        return null;
    }

    @Override
    protected void initView() {
        fm1 = new MVFragment(0);
        fm2 = new MVFragment(1);
        fm3 = new MVFragment(2);
        fm4 = new MVFragment(3);
        fm5 = new MVFragment(4);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.ll_mv, fm1)
                .add(R.id.ll_mv, fm2)
                .add(R.id.ll_mv, fm3)
                .add(R.id.ll_mv, fm4)
                .add(R.id.ll_mv, fm5)
                .show(fm1)
                .setMaxLifecycle(fm1, Lifecycle.State.RESUMED)
                .hide(fm2)
                .setMaxLifecycle(fm2, Lifecycle.State.STARTED)
                .hide(fm3)
                .setMaxLifecycle(fm3, Lifecycle.State.STARTED)
                .hide(fm4)
                .setMaxLifecycle(fm4, Lifecycle.State.STARTED)
                .hide(fm5)
                .setMaxLifecycle(fm5, Lifecycle.State.STARTED)
                .commit();
    }

    @Override
    protected void addListener() {
        binding.rbFm1.setOnClickListener(v -> getSupportFragmentManager().beginTransaction()
                .show(fm1)
                .setMaxLifecycle(fm1, Lifecycle.State.RESUMED)
                .hide(fm2)
                .setMaxLifecycle(fm2, Lifecycle.State.STARTED)
                .hide(fm3)
                .setMaxLifecycle(fm3, Lifecycle.State.STARTED)
                .hide(fm4)
                .setMaxLifecycle(fm4, Lifecycle.State.STARTED)
                .hide(fm5)
                .setMaxLifecycle(fm5, Lifecycle.State.STARTED)
                .commit());
        binding.rbFm2.setOnClickListener(v -> getSupportFragmentManager().beginTransaction()
                .show(fm2)
                .setMaxLifecycle(fm2, Lifecycle.State.RESUMED)
                .hide(fm1)
                .setMaxLifecycle(fm1, Lifecycle.State.STARTED)
                .hide(fm3)
                .setMaxLifecycle(fm3, Lifecycle.State.STARTED)
                .hide(fm4)
                .setMaxLifecycle(fm4, Lifecycle.State.STARTED)
                .hide(fm5)
                .setMaxLifecycle(fm5, Lifecycle.State.STARTED)
                .commit());
        binding.rbFm3.setOnClickListener(v -> getSupportFragmentManager().beginTransaction()
                .show(fm3)
                .setMaxLifecycle(fm3, Lifecycle.State.RESUMED)
                .hide(fm2)
                .setMaxLifecycle(fm2, Lifecycle.State.STARTED)
                .hide(fm1)
                .setMaxLifecycle(fm1, Lifecycle.State.STARTED)
                .hide(fm4)
                .setMaxLifecycle(fm4, Lifecycle.State.STARTED)
                .hide(fm5)
                .setMaxLifecycle(fm5, Lifecycle.State.STARTED)
                .commit());
        binding.rbFm4.setOnClickListener(v -> getSupportFragmentManager().beginTransaction()
                .show(fm4)
                .setMaxLifecycle(fm4, Lifecycle.State.RESUMED)
                .hide(fm2)
                .setMaxLifecycle(fm2, Lifecycle.State.STARTED)
                .hide(fm3)
                .setMaxLifecycle(fm3, Lifecycle.State.STARTED)
                .hide(fm1)
                .setMaxLifecycle(fm1, Lifecycle.State.STARTED)
                .hide(fm5)
                .setMaxLifecycle(fm5, Lifecycle.State.STARTED)
                .commit());
        binding.rbFm5.setOnClickListener(v -> getSupportFragmentManager().beginTransaction()
                .show(fm5)
                .setMaxLifecycle(fm5, Lifecycle.State.RESUMED)
                .hide(fm2)
                .setMaxLifecycle(fm2, Lifecycle.State.STARTED)
                .hide(fm3)
                .setMaxLifecycle(fm3, Lifecycle.State.STARTED)
                .hide(fm4)
                .setMaxLifecycle(fm4, Lifecycle.State.STARTED)
                .hide(fm1)
                .setMaxLifecycle(fm1, Lifecycle.State.STARTED)
                .commit());
    }
}

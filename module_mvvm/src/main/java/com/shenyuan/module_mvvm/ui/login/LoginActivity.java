package com.shenyuan.module_mvvm.ui.login;

import android.content.Intent;

import androidx.annotation.Nullable;

import com.shenyuan.module_mvvm.api.NewsViewModel;
import com.shenyuan.module_mvvm.databinding.ActivityLoginBinding;
import com.shenyuan.superapp.base.base.mvvm.BaseVMActivity;
import com.shenyuan.superapp.base.utils.LogUtils;

public class LoginActivity extends BaseVMActivity<ActivityLoginBinding, NewsViewModel> {

    @Override
    protected NewsViewModel createViewModel() {
        return getScopeViewModel(NewsViewModel.class);
    }

    @Override
    protected void initView() {


    }

    @Override
    protected void addListener() {
//        binding.login.setOnClickListener(v ->
//                viewModel.getRecommendList()
//                        .observe(this, result -> {
//                            if (isSucc(result)) {
//                            }
//                        }));
//        binding.login.setOnClickListener(v -> startActivity(new Intent(context, MVTabActivity.class)));
        binding.login.setOnClickListener(v -> startActivity(new Intent(context, MVListActity.class)));


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.e("");
    }
}
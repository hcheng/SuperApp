package com.shenyuan.module_mvvm.ui.login;

import android.graphics.Color;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.google.android.material.tabs.TabLayout;
import com.shenyuan.module_mvvm.R;
import com.shenyuan.module_mvvm.bean.TabBean;
import com.shenyuan.module_mvvm.databinding.ActivityMvListBinding;
import com.shenyuan.module_mvvm.databinding.ItemMvListBinding;
import com.shenyuan.module_mvvm.databinding.ItemTabBinding;
import com.shenyuan.superapp.base.base.mvvm.BaseVMActivity;
import com.shenyuan.superapp.base.base.mvvm.BaseViewModel;
import com.shenyuan.superapp.base.widget.recy.CenterLayoutManager;
import com.shenyuan.superapp.base.widget.recy.scroll.LinearLayoutManagerWithScrollTop;
import com.shenyuan.superapp.common.base.BaseVBAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ch
 * @date 2022/8/22-17:34
 * desc
 */
public class MVListActity extends BaseVMActivity<ActivityMvListBinding, BaseViewModel> {
    private LinearLayoutManagerWithScrollTop layoutManager;
    private int index;
    private MvTabAdapter tabAdapter;


    private boolean clickMove;

    @Override
    protected BaseViewModel createViewModel() {
        return null;
    }

    @Override
    protected void initView() {
        List<TabBean> tablist = new ArrayList<>();
        tablist.add(new TabBean("文本1"));
        tablist.add(new TabBean("文本2"));
        tablist.add(new TabBean("文本3"));
        tablist.add(new TabBean("文本4"));
        tablist.add(new TabBean("文本5"));
        tablist.add(new TabBean("文本6"));
        tablist.add(new TabBean("文本7"));
        tablist.add(new TabBean("文本8"));
        tablist.add(new TabBean("文本9"));
        tablist.add(new TabBean("文本10"));
        tablist.add(new TabBean("文本11"));
        tablist.add(new TabBean("文本12"));
        tablist.add(new TabBean("文本13"));

        for (TabBean str : tablist) {
            binding.mvTab.addTab(binding.mvTab.newTab().setText(str.getText()));
        }
        MvAdapter adapter = new MvAdapter();
        layoutManager = new LinearLayoutManagerWithScrollTop(context);
        binding.rvMv.setLayoutManager(layoutManager);
        binding.rvMv.setAdapter(adapter);

        adapter.setNewInstance(tablist);

        tabAdapter = new MvTabAdapter();
        binding.rvTab.setLayoutManager(new CenterLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        binding.rvTab.setAdapter(tabAdapter);
        tabAdapter.setNewInstance(tablist);

    }

    @Override
    protected void addListener() {
        binding.mvTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                index = tab.getPosition();
                clickMove = true;
                binding.rvMv.smoothScrollToPosition(index);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        binding.rvMv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    clickMove = false;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!clickMove) {
                    int position = layoutManager.findFirstVisibleItemPosition();
                    setSelect(position);
                    binding.rvTab.smoothScrollToPosition(position);

                    binding.mvTab.setScrollPosition(position, 0, true, true);
                }

            }
        });

        tabAdapter.setClickListener((bean, view, position) -> {
            clickMove = true;
            setSelect(position);
            binding.rvMv.smoothScrollToPosition(position);
        });
    }

    private void setSelect(int position) {
        for (TabBean tabBean : tabAdapter.getData()) {
            if (tabAdapter.getData().indexOf(tabBean) == position) {
                tabBean.setSelect(true);
            } else {
                tabBean.setSelect(false);
            }
        }
        tabAdapter.notifyDataSetChanged();
    }

    public class MvAdapter extends BaseVBAdapter<TabBean, ItemMvListBinding> {
        public MvAdapter() {
            super(R.layout.item_mv_list);
        }

        @Override
        protected void convert(@NonNull BaseDataBindingHolder<ItemMvListBinding> holder, TabBean s) {
            ItemMvListBinding binding = holder.getDataBinding();
            if (binding == null) {
                return;
            }
            binding.tv1.setText(s.getText());
        }
    }

    public class MvTabAdapter extends BaseVBAdapter<TabBean, ItemTabBinding> {
        public MvTabAdapter() {
            super(R.layout.item_tab);
        }

        @Override
        protected void convert(@NonNull BaseDataBindingHolder<ItemTabBinding> holder, TabBean tabBean) {
            ItemTabBinding binding = holder.getDataBinding();
            if (binding == null) {
                return;
            }
            binding.tvTab.setText(tabBean.getText());
            binding.tvTab.setEnabled(tabBean.isSelect());
        }
    }
}

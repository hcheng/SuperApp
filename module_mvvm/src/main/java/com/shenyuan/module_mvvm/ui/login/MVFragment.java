package com.shenyuan.module_mvvm.ui.login;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.shenyuan.module_mvvm.R;
import com.shenyuan.module_mvvm.adapter.NewsListAdapter;
import com.shenyuan.module_mvvm.api.NewsViewModel;
import com.shenyuan.module_mvvm.bean.NewsBean;
import com.shenyuan.module_mvvm.databinding.FmMvvmBinding;
import com.shenyuan.superapp.base.base.mvvm.BaseVMFragment;
import com.shenyuan.superapp.base.widget.recy.DividerDecoration;
import com.shenyuan.superapp.common.web.WebActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ch
 * @date 2022/6/13-14:46
 * desc
 */
public class MVFragment extends BaseVMFragment<FmMvvmBinding, NewsViewModel> {

    private int typeId;
    private NewsListAdapter newsListAdapter;

    public MVFragment(int typeId) {
        this.typeId = typeId;
    }

    private int page = 1;

    @Override
    protected NewsViewModel createViewModel() {
        return getScopeViewModel(NewsViewModel.class);
    }

    @Override
    protected void loadData() {
        viewModel.getNewList(typeId, page).observe(this, listResult -> {
            List<NewsBean> beanList = new ArrayList<>();

            for (int i = 0; i < 10; i++) {
                NewsBean bean = new NewsBean();
                bean.setTitle("标题" + i);
                bean.setDigest("内容" + i);
                beanList.add(bean);
            }
//            if (isSucc(listResult)) {
            if (page == 1) {
                newsListAdapter.setNewInstance(beanList);
            } else {
                newsListAdapter.addData(beanList);
            }
//            }
            binding.mrlNews.finishRefreshAndLoadMore();
        });
    }

    @Override
    protected void initView() {
        newsListAdapter = new NewsListAdapter();
        binding.rvNews.setLayoutManager(new LinearLayoutManager(context));
        binding.rvNews.setAdapter(newsListAdapter);
        binding.rvNews.addItemDecoration(new DividerDecoration(getResources().getColor(R.color.color_e5e5e5), 2));

    }

    @Override
    protected void addListener() {
        binding.mrlNews.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                page++;
                loadData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 1;
                loadData();
            }
        });

        newsListAdapter.setClickListener((bean, view, position) ->
                viewModel.getNewDetails(bean.getNewsId()).observe(this, result -> {
                    if (isSucc(result)) {
                        WebActivity.loadText(context, result.getData().getContent());
                    }
                }));

    }
}

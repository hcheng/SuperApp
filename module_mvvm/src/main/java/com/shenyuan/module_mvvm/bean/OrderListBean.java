package com.shenyuan.module_mvvm.bean;

/**
 * @author ch
 * @date 2022/6/21-14:29
 * desc
 */
public class OrderListBean {
    private String name;
    private Integer type;
    private String comment;
    private String picS192;
    private String picS444;
    private String picS260;
    private String picS210;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPicS192() {
        return picS192;
    }

    public void setPicS192(String picS192) {
        this.picS192 = picS192;
    }

    public String getPicS444() {
        return picS444;
    }

    public void setPicS444(String picS444) {
        this.picS444 = picS444;
    }

    public String getPicS260() {
        return picS260;
    }

    public void setPicS260(String picS260) {
        this.picS260 = picS260;
    }

    public String getPicS210() {
        return picS210;
    }

    public void setPicS210(String picS210) {
        this.picS210 = picS210;
    }
}

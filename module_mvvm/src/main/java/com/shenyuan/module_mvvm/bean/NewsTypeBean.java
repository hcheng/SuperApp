package com.shenyuan.module_mvvm.bean;

/**
 * @author ch
 * @date 2022/6/21-14:56
 * desc
 */
public class NewsTypeBean {
    private Integer typeId;
    private String typeName;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}

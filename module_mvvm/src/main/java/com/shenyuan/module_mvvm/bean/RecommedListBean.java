package com.shenyuan.module_mvvm.bean;

import java.io.Serializable;

/**
 * @author ch
 * @date 2022/6/21-11:50
 * desc
 */

public class RecommedListBean implements Serializable {

    private String picHuge;
    private String tingUid;
    private String siProxycompany;
    private String author;
    private String info;
    private String albumTitle;
    private String title;
    private String language;
    private String picBig;
    private String picSinger;
    private String publishtime;
    private String picPremium;
    private String songId;
    private String picSmall;

    public String getPicHuge() {
        return picHuge;
    }

    public void setPicHuge(String picHuge) {
        this.picHuge = picHuge;
    }

    public String getTingUid() {
        return tingUid;
    }

    public void setTingUid(String tingUid) {
        this.tingUid = tingUid;
    }

    public String getSiProxycompany() {
        return siProxycompany;
    }

    public void setSiProxycompany(String siProxycompany) {
        this.siProxycompany = siProxycompany;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPicBig() {
        return picBig;
    }

    public void setPicBig(String picBig) {
        this.picBig = picBig;
    }

    public String getPicSinger() {
        return picSinger;
    }

    public void setPicSinger(String picSinger) {
        this.picSinger = picSinger;
    }

    public String getPublishtime() {
        return publishtime;
    }

    public void setPublishtime(String publishtime) {
        this.publishtime = publishtime;
    }

    public String getPicPremium() {
        return picPremium;
    }

    public void setPicPremium(String picPremium) {
        this.picPremium = picPremium;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getPicSmall() {
        return picSmall;
    }

    public void setPicSmall(String picSmall) {
        this.picSmall = picSmall;
    }
}

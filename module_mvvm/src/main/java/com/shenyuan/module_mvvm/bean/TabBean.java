package com.shenyuan.module_mvvm.bean;

/**
 * @author ch
 * @date 2022/8/23-11:31
 * desc
 */
public class TabBean {
    private String text;
    private boolean select;

    public TabBean(String text) {
        this.text = text;
    }

    public TabBean(String text, boolean select) {
        this.text = text;
        this.select = select;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}

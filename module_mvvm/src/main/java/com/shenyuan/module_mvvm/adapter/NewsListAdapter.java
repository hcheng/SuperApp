package com.shenyuan.module_mvvm.adapter;

import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.shenyuan.module_mvvm.R;
import com.shenyuan.module_mvvm.bean.NewsBean;
import com.shenyuan.module_mvvm.databinding.ItemNewsBinding;
import com.shenyuan.superapp.common.base.BaseVBAdapter;

/**
 * @author ch
 * @date 2022/6/21-15:14
 * desc
 */
public class NewsListAdapter extends BaseVBAdapter<NewsBean, ItemNewsBinding> {
    public NewsListAdapter() {
        super(R.layout.item_news);
    }

    @Override
    protected void convert(@NonNull BaseDataBindingHolder<ItemNewsBinding> holder, NewsBean bean) {
        ItemNewsBinding binding = holder.getDataBinding();
        if (binding == null) {
            return;
        }
        binding.tvTitle.setText(bean.getTitle());
        binding.tvDes.setText(bean.getDigest());
        if (TextUtils.isEmpty(bean.getDigest())){
            binding.tvDes.setVisibility(View.GONE);
        }else {
            binding.tvDes.setVisibility(View.VISIBLE);
        }
        binding.tvSource.setText(bean.getSource());
        binding.tvTime.setText(bean.getPostTime());
    }
}

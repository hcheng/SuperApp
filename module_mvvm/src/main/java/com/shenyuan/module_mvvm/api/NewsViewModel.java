package com.shenyuan.module_mvvm.api;

import com.shenyuan.module_mvvm.bean.NewsBean;
import com.shenyuan.module_mvvm.bean.NewsDetailBean;
import com.shenyuan.module_mvvm.bean.NewsTypeBean;
import com.shenyuan.module_mvvm.bean.OrderListBean;
import com.shenyuan.module_mvvm.bean.RecommedListBean;
import com.shenyuan.superapp.base.api.ApiRetrofit;
import com.shenyuan.superapp.base.base.mvvm.BaseLiveData;
import com.shenyuan.superapp.base.base.mvvm.BaseResult;
import com.shenyuan.superapp.base.base.mvvm.BaseVMSubscriber;
import com.shenyuan.superapp.base.base.mvvm.BaseViewModel;

import java.util.List;

/**
 * @author ch
 * @date 2022/6/7-18:06
 * desc
 */
public class NewsViewModel extends BaseViewModel {

    private MVApiServer mvApiServer = ApiRetrofit.getInstance().getService(MVApiServer.class);

    public BaseLiveData<BaseResult<List<RecommedListBean>>> getRecommendList() {
        return addDisposable(mvApiServer.getRecommendList(), new BaseVMSubscriber<>(false));
    }

    /**
     * 获取每日音乐推荐列表
     *
     * @return
     */
    public BaseLiveData<BaseResult<List<OrderListBean>>> getOrderList() {
        return addDisposable(mvApiServer.getOrderList(), new BaseVMSubscriber<>(false));
    }

    /**
     * 获取所有新闻类型列表
     *
     * @return
     */
    public BaseLiveData<BaseResult<List<NewsTypeBean>>> getNewsType() {
        return addDisposable(mvApiServer.getNewsType(), new BaseVMSubscriber<>(false));
    }

    /**
     * 获取所有新闻类型列表
     *
     * @return
     */
    public BaseLiveData<BaseResult<List<NewsBean>>> getNewList(int typeId, int page) {
        return addDisposable(mvApiServer.getNewList(typeId, page), new BaseVMSubscriber<>(true));
    }

    /**
     * 获取所有新闻类型列表
     *
     * @return
     */
    public BaseLiveData<BaseResult<NewsDetailBean>> getNewDetails(String newsId) {
        return addDisposable(mvApiServer.getNewDetails(newsId), new BaseVMSubscriber<>(true));
    }
}

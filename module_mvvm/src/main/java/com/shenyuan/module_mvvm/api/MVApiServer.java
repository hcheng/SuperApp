package com.shenyuan.module_mvvm.api;


import com.shenyuan.module_mvvm.bean.NewsBean;
import com.shenyuan.module_mvvm.bean.NewsDetailBean;
import com.shenyuan.module_mvvm.bean.NewsTypeBean;
import com.shenyuan.module_mvvm.bean.OrderListBean;
import com.shenyuan.module_mvvm.bean.RecommedListBean;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author ch
 * @date 2020/4/21-10:20
 * desc ApiServer
 */
public interface MVApiServer {
    /**
     * 刷新token
     *
     * @param map map
     * @return Call
     */
    @FormUrlEncoded
    @POST("auth/oauth/token")
    Call<HashMap<String, String>> refreshToken(@FieldMap HashMap<String, String> map);

    /**
     * 获取每日音乐推荐列表
     *
     * @return Call
     */
    @GET("music/recommend/list")
    Flowable<List<RecommedListBean>> getRecommendList();

    /**
     * 获取每日音乐推荐列表
     *
     * @return Call
     */
    @GET("music/order/list")
    Flowable<List<OrderListBean>> getOrderList();

    /**
     * 获取所有新闻类型列表
     *
     * @return
     */
    @GET("news/types")
    Flowable<List<NewsTypeBean>> getNewsType();

    /**
     * 获取所有新闻类型列表
     *
     * @return
     */
    @GET("news/list")
    Flowable<List<NewsBean>> getNewList(@Query("typeId") int typeId, @Query("page") int page);
    /**
     * 根据新闻id获取新闻详情
     *
     * @return
     */
    @GET("news/details")
    Flowable<NewsDetailBean> getNewDetails(@Query("newsId") String newsId);
}

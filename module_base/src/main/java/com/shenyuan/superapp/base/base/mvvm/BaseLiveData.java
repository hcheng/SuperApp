package com.shenyuan.superapp.base.base.mvvm;

import androidx.lifecycle.MutableLiveData;

/**
 * @author ch
 * @date 2022/6/7-18:21
 * desc
 */
public class BaseLiveData<T> extends MutableLiveData<T> {

}

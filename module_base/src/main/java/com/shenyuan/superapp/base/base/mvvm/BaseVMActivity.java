package com.shenyuan.superapp.base.base.mvvm;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.Toast;

import androidx.activity.ComponentActivity;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.alibaba.android.arouter.launcher.ARouter;
import com.shenyuan.superapp.base.R;
import com.shenyuan.superapp.base.utils.LogUtils;
import com.shenyuan.superapp.base.widget.GifLoadingView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author ch
 * @date 2022/6/7-17:43
 * desc
 */
public abstract class BaseVMActivity<B extends ViewDataBinding, VM extends BaseViewModel> extends FragmentActivity
        implements LifecycleOwner, ViewModelStoreOwner {

    /**
     * context
     */
    public Context context;

    /**
     * binding
     */
    public B binding;

    /**
     * viewModel
     */
    public VM viewModel;

    /**
     *
     */
    private ViewModelProvider viewModelProvider;

    /**
     * loading
     */
    private GifLoadingView loadingView;

    /**
     * 创建viewModel
     *
     * @return VM
     */
    protected abstract VM createViewModel();

    /**
     * 初始化view
     */
    protected abstract void initView();

    /**
     * 添加监听
     */
    protected abstract void addListener();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setStatusBar();
        ARouter.getInstance().inject(this);
        binding = getBinding();
        if (binding != null) {
            setContentView(binding.getRoot());
        }
        viewModel = createViewModel();
        initView();
        addListener();
    }

    /**
     * 设置状态栏
     */
    protected void setStatusBar() {

    }

    protected <T extends ViewModel> T getScopeViewModel(@NonNull Class<T> modelClass) {
        if (viewModelProvider == null) {
            viewModelProvider = new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication()));
        }
        return viewModelProvider.get(modelClass);
    }


    protected <T> boolean isSucc(@NonNull BaseResult<T> result) {
        LogUtils.e("code=" + result.getCode() + ",msg=" + result.getMsg());

        if (BaseResult.CODE_SHOW_LODING.equals(result.getCode())) {
            //显示加载动画
            showLoadingDialog();
            return false;
        } else if (!BaseResult.CODE_SUCC.equals(result.getCode())) {
            showToast(result.getMsg());
            closeLoadingDialog();
            return false;
        }
        closeLoadingDialog();
        return true;
    }

    protected void closeLoadingDialog() {
        if (loadingView != null) {
            loadingView.onDismiss(null);
        }
    }


    protected void showLoadingDialog() {
        if (loadingView == null) {
            loadingView = new GifLoadingView();
            loadingView.setImageResource(R.mipmap.num19);
        }
        loadingView.show(getSupportFragmentManager());
    }


    /**
     * @param s s
     */
    public void showToast(@NonNull String s) {
        Toast toast = Toast.makeText(context, s, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * 反射获取 ViewDataBinding
     *
     * @return ViewDataBinding
     */
    private B getBinding() {
        Class classz = getBingingClass();
        if (classz == null) {
            return null;
        }
        try {
            return (B) classz.getMethod("inflate", LayoutInflater.class).invoke(null, LayoutInflater.from(context));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取Binging的泛型
     *
     * @return Class
     */
    private Class getBingingClass() {
        if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
            Type[] types = parameterizedType.getActualTypeArguments();
            if (types.length > 0) {
                return (Class) types[0];
            }
        }
        return null;
    }

}

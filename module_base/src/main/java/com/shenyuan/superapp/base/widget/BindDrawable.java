package com.shenyuan.superapp.base.widget;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.databinding.BindingAdapter;

/**
 * @author ch
 * @date 2022/6/16-18:05
 * desc 圆角、描边等
 */
public class BindDrawable {
    @BindingAdapter(value = {
            "bing_color",
            "bing_radius",
            "bing_radius_left_top",
            "bing_radius_left_bottom",
            "bing_radius_right_top",
            "bing_radius_right_bottom",
            "bing_stroke_color",
            "bing_stroke_width",
            "bing_stroke_dash",
            "bing_stroke_dashGap",
            "bing_start_color",
            "bing_center_color",
            "bing_end_color",
            "bing_enabled_color",
            "bing_enabled_strokeColor",
            "bing_enabled_strokeWidth",
            "bing_enabled_strokeDash",
            "bing_enabled_strokeDashGap",
            "bing_enabled_radius",
            "bing_enabled_radius_left_top",
            "bing_enabled_radius_left_bottom",
            "bing_enabled_radius_right_top",
            "bing_enabled_radius_right_bottom",
            "bing_enabled_start_color",
            "bing_enabled_center_color",
            "bing_enabled_end_color"

    }, requireAll = false)
    public static void setViewBackground(View view, @ColorInt int color, int radius, int radiusLeftTop,
                                         int radiusLeftBottom, int radiusRightTop, int radiusRightBottom,
                                         @ColorInt int strokeColor, int strokeWidth, int strokeDash, int strokeDashGap,
                                         @ColorInt int startColor, @ColorInt int centerColor, @ColorInt int endColor,
                                         @ColorInt int enabledColor, @ColorInt int enabledStrokeColor, int enabledStrokeWidth, int enabledStrokeDash,
                                         int enabledStrokeDashGap, int enabledRadius, int enabledRadiusLeftTop, int enabledRadiusLeftBottom,
                                         int enabledRadiusRightTop, int enabledRadiusRightBottom, @ColorInt int enabledStartColor,
                                         @ColorInt int enabledCenterColor, @ColorInt int enabledEndColor) {


        Drawable defaultDrawable = create(color, strokeColor, strokeWidth, radius, radiusLeftTop, radiusLeftBottom, radiusRightTop, radiusRightBottom, startColor, centerColor, endColor, strokeDash, strokeDashGap);

        int count = 1;

        Drawable enabledDrawable = null;
        if (enabledColor != 0) {
            enabledDrawable = create(enabledColor, enabledStrokeColor, enabledStrokeWidth, enabledRadius, enabledRadiusLeftTop, enabledRadiusLeftBottom,
                    enabledRadiusRightTop, enabledRadiusRightBottom, enabledStartColor, enabledCenterColor, enabledEndColor, enabledStrokeDash, enabledStrokeDashGap);
            count++;
        }

        if (count == 1) {
            view.setBackground(defaultDrawable);
        } else {
            ProxyDrawable listDrawable = new ProxyDrawable();
            if (enabledDrawable != null) {
                listDrawable.addState(new int[]{android.R.attr.state_enabled}, enabledDrawable);
            }
            listDrawable.addState(new int[]{0}, defaultDrawable);

            view.setBackground(listDrawable);
        }
    }

    public static Drawable create(
            @ColorInt Integer solidColor,
            @ColorInt int strokeColor, float strokeWidth,
            float radius, float radiusLt, float radiusLb, float radiusRt, float radiusRb,
            @ColorInt Integer startColor, @ColorInt Integer centerColor, @ColorInt Integer endColor,
            float strokeDash, float strokeDashGap) {

        GradientDrawable drawable = new GradientDrawable();
        if (startColor != null && startColor != 0 && endColor != null) {
            int[] colors;
            if (centerColor != null) {
                colors = new int[3];
                colors[0] = startColor;
                colors[1] = centerColor;
                colors[2] = endColor;
            } else {
                colors = new int[2];
                colors[0] = startColor;
                colors[1] = endColor;
            }
            drawable.setColors(colors);
        } else {
            if (solidColor != null) {
                drawable.setColor(solidColor);
            }
        }
        if (strokeWidth > 0) {
            drawable.setStroke(dip2px(strokeWidth), strokeColor, dip2px(strokeDash), dip2px(strokeDashGap));
        }
        if (radius <= 0) {
            float[] radiusEach = new float[]{dip2px(radiusLt), dip2px(radiusLt), dip2px(radiusRt), dip2px(radiusRt),
                    dip2px(radiusRb), dip2px(radiusRb), dip2px(radiusLb), dip2px(radiusLb)};
            drawable.setCornerRadii(radiusEach);
        } else {
            drawable.setCornerRadius(dip2px(radius));
        }
        return drawable;
    }

    private static int dip2px(float dipValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dipValue * scale + .5f);
    }


    public static class ProxyDrawable extends StateListDrawable {

        private Drawable originDrawable;

        @Override
        public void addState(int[] stateSet, Drawable drawable) {
            if (stateSet != null && stateSet.length == 1 && stateSet[0] == 0) {
                originDrawable = drawable;
            }
            super.addState(stateSet, drawable);
        }

        Drawable getOriginDrawable() {
            return originDrawable;
        }
    }
}
